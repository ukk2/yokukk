import React from "react";
import PropTypes from "prop-types";
import ProgressBar from "../ProgressBar";
// import { RiMoneyCnyCircleLine } from "react-icons/Fa";

export default function CountCard({ name, image, stock, price, onCart, onClick }) {
  return (
    <div>
      <div class="d-flex justify-content-evenly">
        <div class="p-2 me-3 bd-highlight bg-warning flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon <RiMoneyCnyCircleLine /> */}
        </div>
        <div class="p-2 me-3 bd-highlight bg-primary flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
        <div class="p-2 me-3 bd-highlight bg-warning flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
        <div class="p-2 bd-highlight bg-danger flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
      </div>
      {/*  */}
      <div class="d-flex justify-content-evenly mt-3">
        <div class="p-2 me-3 bd-highlight bg-warning flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
        <div class="p-2 me-3 bd-highlight bg-primary flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
        <div class="p-2 me-3 bd-highlight bg-warning flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
      </div>
      {/*  */}
      <div class="d-flex justify-content-evenly mt-3">
        <div class="p-2 me-3 bd-highlight bg-warning ">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
        <div class="p-2 bd-highlight bg-primary flex-fill">
          <h6>Total Transaksi</h6>
          <h1>Rp.123.000.00</h1>
          {/* icon */}
        </div>
      </div>
    </div>
  );
}

CountCard.defaultProps = {
  name: "",
  value: null,
  onClick: () => {},
};

CountCard.propTypes = {
  name: PropTypes.string,
  value: PropTypes.number,
  onClick: PropTypes.func,
};
