import React from "react";
import PropTypes from "prop-types";

export default function ProgressBar({ value, now }) {
  return (
    <div>
      <div class="progress">
        <div class="progress-bar progress-bar-striped bg-info w-75 p-3" role="progressbar" width={`${now}%`} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      {/* <ProgressBar now={now} label={`${now}%`} />; */}
    </div>
  );
}

// call iniasi
ProgressBar.defaultProps = {
  name: "",
  // value: 30,
  now: 30,
  value: null,
  onClick: () => {},
};
ProgressBar.propTypes = {
  name: PropTypes.string,
  // value: PropTypes.number,
  now: PropTypes.number,
  onClick: PropTypes.func,
};
