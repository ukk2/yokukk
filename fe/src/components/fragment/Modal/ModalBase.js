// inisiasi library default
import React from "react";
import { useNavigate, useLocation } from "react-router-dom";
import $ from "jquery";
import queryString from "querystring-es3";

// function
export default function ModalBase({ header, children, onClick, confirmText }) {
  // deklarasi variabel di function component
  let navigate = useNavigate();
  const location = useLocation();
  const { id } = queryString.parse(location.search.replace("?", "")); //eutk replace data menjadi string

  const handleClose = () => {
    if (id)
      navigate({
        search: queryString.stringify({
          id,
        }),
      });
    $(`#${id}`).modal("hide");
  };

  return <div>hshs</div>;
}
