import React from "react";
import Dashboard from "../../components/Dashboard";
import ListTable from "../../components/elements/ListTable";
// import CountCard from "../../components/elements/CountCard";

export default function Transaksi() {
  return (
    <Dashboard>
      <h1>Hai Transaksi</h1>
      <ListTable />
    </Dashboard>
  );
}
