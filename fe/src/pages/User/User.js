// inisiasi library default
import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import queryString from "querystring-es3";
import moment from "moment";

// inisiasi component
import Dashboard from "../../components/Dashboard";
import ListTable from "../../components/elements/ListTable";
import { getAll, editUser, deleteOne } from "./action";

// inisiasi icon
import { BiDetail, BiEdit } from "react-icons/bi";
import { CgCloseR } from "react-icons/cg";

// function
export default function User() {
  // deklarasi variabel di function component
  let navigate = useNavigate();
  const [response, setResponse] = useState({});
  const location = useLocation();
  const { id } = queryString.parse(location.search.replace("?", ""));

  useEffect(() => {
    getAll(setResponse);
  }, [id]);

  // useEffect seperti componentDidMount dan componentDidUpdate
  // useEffect(() => {
  //    memperbarui judul dokumen menggunakan API browser
  //   if (response.message === 'token tidak valid') history.push('/login');
  // }, [response]);

  // const renderDate = (date) => {
  //   if (date !== "") return moment(date).locale("id").format("D MMMM YYYY");
  //   else return moment().locale("id").format("D MMMM YYYY");
  // };

  const renderAction = (userId) => {
    return (
      <div className="d-grid gap-2 d-md-flex justify-content-md-start">
        <span
          onClick={() => {
            console.log(userId);
            navigate({
              search: queryString.stringify({
                id: userId,
              }),
            });
            // openModal("editUser");
          }}
          className="btn btn-info text-white d-flex justify-content-center align-items-center "
        >
          <BiDetail className="me-2" /> Info Detail
        </span>
        {/* jjs */}
        <span
          onClick={() => {
            console.log(userId);
            navigate({
              search: queryString.stringify({
                id: userId,
              }),
            });
            // openModal("editUser");
          }}
          className="btn btn-primary text-white d-flex justify-content-center align-items-center "
        >
          <BiEdit className="text-bold" />
        </span>
        {/* bisa hapus kl sebagai admin */}
        <span
          onClick={() => {
            console.log(userId);
            navigate({
              search: queryString.stringify({
                id: userId,
              }),
            });
            // openModal("editUser");
          }}
          className="btn btn-danger text-white d-flex justify-content-center align-items-center "
        >
          <CgCloseR className="" />
        </span>
      </div>
    );
  };

  const column = [
    {
      heading: "Nama",
      value: (v) => <p className="font-bold">{v.nama}</p>,
    },
    {
      heading: "Username",
      value: (v) => v.username,
    },
    // {
    //   heading: "Foto",
    //   value: (v) => v.image,
    // },
    // {
    //   heading: "Password",
    //   value: (v) => v.password,
    // },
    {
      heading: "Role",
      value: (v) => v.role,
    },
    // {
    //   heading: "tgl bergabung",
    //   value: (v) => renderDate(v.createAt_masyarakat),
    // },
    {
      heading: "",
      value: (v) => renderAction(v.id_user),
    },
  ];

  return (
    <Dashboard>
      <h1>Hai Petugas = User</h1>
      {/* card */}
      <div class="card">
        {/* <div class="card-header">
          <div className="d-flex justify-content-start mt-2 mb-2">
            <button type="button" class="btn btn-primary" disabled>
              Total: 89
            </button>
            <button type="button" class="ms-3 btn btn-primary">
              Download Document
            </button>
          </div>
        </div> */}
        <div class="card-body">
          <ListTable column={column} data={response.data} className="table w-full" />
        </div>
      </div>
      {/* <ListTable /> */}
      <div>
        {/* <p>You clicked {count} times</p>
        <button onClick={() => setCount(count + 1)}>Click me</button> */}
        {/* <ListTable column={column} data={response.data} className="table w-full" /> */}
      </div>
    </Dashboard>
  );
}
