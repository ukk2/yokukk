import React from "react";
import Dashboard from "../../components/Dashboard";
import CountCard from "../../components/elements/CountCard";
import ListCard from "../../components/elements/ListCard";

export default function Outlet() {
  return (
    <Dashboard>
      <h1>Hai Outlet</h1>
      {/* <CountCard name={"Hai apa"} value={"tempat"} className=" " /> */}
      <ListCard />
    </Dashboard>
  );
}
